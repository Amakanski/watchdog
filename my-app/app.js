var http = require('http');
var mysql = require('mysql');
var child_process = require('child_process');
var is_running = require('is-running');

var settings = {
    "mysql": {
        "host": "localhost",
        "user": "root",
        "password": "",
        "database": "watchdog"
    },
    "polling_interval": 10000
};
    
// require('./settings.json').production;

console.log('Listening on port: ', process.env.PORT);

var mysql_connection = mysql.createConnection(settings.mysql);

mysql_connection.connect();

var thread_list = function(callback){
    mysql_connection.query("SELECT * FROM threads", function(err, rows, fields){
        if(err) throw err;

        callback(rows);
    });
}

var thread_save = function(thread){
    sql = "UPDATE threads SET pid = ? WHERE id = ?";
    mysql_connection.query(sql, [ thread.pid, thread.id ]);
}

var pipe_write = function(thread, pipe, text){
    var sql = "INSERT INTO pipes (thread, pid, pipe, text) VALUES (?, ?, ?, ?)";
    var values = [thread.id, thread.pid, pipe, text];
    mysql_connection.query(sql, values, function(err, result){
        // Error handler... ??
        if(err) throw err;
    });
}

var thread_exists = function(number){
    return is_running(number);
}

var thread_start = function(thread){
    
    pipe_write(thread, '', 'Starting: '+ thread.command);

    var args = [];
    if(thread.arguments != null)
        try {
            args = JSON.parse(thread.arguments);
            pipe_write(thread, '', 'Arguments: '+ JSON.stringify(args));
        } catch(e){
            pipe_write(thread, '', 'No arguments or unable to parse arguments');
            console.log(e);
        }

    var process = child_process.spawn(thread.command, args);

    pipe_write(thread, '', 'PID: '+ process.pid);
    
    thread.pid = process.pid;
    thread_save(thread);

    process.on('error', function(err){
        pipe_write(thread, 'error', 'Could not spawn process ('+ err.code +')');
    });
    process.stdout.on('message', function(message, sendHandle){
        pipe_write(thread, 'message', message);
    });
    process.stdout.on('data', function(data){
        pipe_write(thread, 'stdout', data.toString('utf8'));
    });
    process.stderr.on('data', function(data){
        pipe_write(thread, 'stderr', data.toString('utf8'));
    });
    process.on('exit', function(code, signal){       
        if(code == 0)
            pipe_write(thread, '', 'Closed connection');
        else
            pipe_write(thread, '', 'Closed connection with code: '+ code +', and signal: '+ signal);

        thread.pid = null;
        thread_save(thread);
    });

}

// Main loop

var thread_start_polling = function(){
    thread_list(function(list){
        list.forEach(function(thread){
            if(thread.pid == null || !thread_exists(thread.pid))
                thread_start(thread);
        });
    });

    setTimeout(thread_start_polling, settings.polling_interval);
}

thread_start_polling();

// Webserver

var server = http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
});

server.listen(0, '0.0.0.0', function(){
    console.log('NodeJS App running...');
    // process.env.PORT}
});